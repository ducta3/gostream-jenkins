cd <directory>
cat pass-deploy.txt | docker login registry.gitlab.com -u <username> --password-stdin
docker stop <name container>
docker rm <name container>
docker pull <image>
docker logout registry.gitlab.com
docker run -d -p 8080:3000 --name <name container> <image>