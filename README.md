# CI-CD-with-jenkins-docker

Example for Jenkins

## Files

- /
  - server.js         Example Express web server.
  - .dockerignore     Make Docker ignore specified files.
  - Dockerfile        Script file for building our Docker image.
  - package.json      Node.js package file, specifies npm dependencies.
  - guideline.txt     Guide line to use Jenkins
  - Jenkinsfile.demo  Example Jenkinsfile
  - sample-deploy.sh  Sample file sh to deploy on server